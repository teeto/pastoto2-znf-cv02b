<?php

namespace App\Model;

use Nette\InvalidArgumentException;
use Tracy\Debugger;


class CompanyModel extends BaseModel
{
    /**
     * Metoda vrací seznam všech firem seřazené podle jména
     */
    public function listCompanies()
    {
        $selection = $this->database->table("company")->order("name DESC");
        return $selection;
    }

    /**
     * Metoda vrací vloží novou firmu
     * @param array $values
     * @return $id vložené firmy
     */
    public function insertCompany($values)
    {
        if($this->checkValidInput($values)){
                return $this->database->table("company")->insert($values);
        }
    }

    /**
     * Metoda edituje firmu, pokud neexistuje vrací NoDataFound.
     * @param array $values
     */
    public function updateCompany($id, $values)
    {
        if($this->checkValidInput($values)){
            return $this->getCompany($id)->update($values);
        }
    }

    /**
     * Metoda odebere firmu, pokud neexistuje vrací NoDataFound.
     * @param array $values
     */
    public function deleteCompany($id)
    {
        $this->getCompany($id)->delete();
    }

    /**
     * Metoda vrací firmu se zadaným id, pokud neexistuje vrací NoDataFound.
     * @param int $id
     */
    public function getCompany($id)
    {
        $selection = $this->database->table("company")->get($id);
        if (!$selection) {
            throw new NoDataFound("No data found");
        }
        return $selection;
    }

    private function checkValidInput($values){
        if (preg_match('/^[a-zá-ž ]*+$/i', $values->name)) {
            if (preg_match('/^[+0-9 ]+/', $values->phone)) {
                $values['registered'] = date('Y-m-d H:i:s');
                return true;
            } else {
                throw new InvalidArgumentException("Telefonní číslo obsahuje nepovolené znaky.");
            }
        } else {
            throw new InvalidArgumentException("Jméno obsahuje nepovolené znaky.");
        }
    }
}