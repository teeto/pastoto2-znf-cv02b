<?php

namespace App\Model;

use Nette\InvalidArgumentException;
use Tracy\Debugger;


class EmployerModel extends BaseModel
{
    /**
     * Metoda vrací seznam všech zaměstanců řazené podle příjmení
     */
    public function listEmployers()
    {
        return $this->database->table('employer');
    }

    /**
     * Metoda vrací zaměstnace se zadaným id, pokud neexistuje vrací NoDataFound.
     * @param int  $id
     */
    public function getEmployer($id)
    {
        $selection = $this->database->table("employer")->get($id);
        if (!$selection) {
            throw new NoDataFound("No data found");
        }
        return $selection;
    }

    /**
     * Metoda vrací vloží nového zaměstnance
     * @param array  $values
     * @return $id vloženého zaměstnance
     */
    public function insertEmployer($values)
    {
        if($this->checkValidInput($values)){
            return  $this->database->table("employer")->insert($values);
        }
    }

    /**
     * Metoda edituje zaměstance, pokud neexistuje vrací NoDataFound.
     * @param int  $id zamestnance
     * @param array  $values
     */
    public function updateEmployer($id, $values)
    {
        if($this->checkValidInput($values)){
            return  $this->getEmployer($id)->update($values);
        }
    }

    /**
     * Metoda odebere zaměstnance, pokud neexistuje vrací NoDataFound.
     * @param array  $values
     */
    public function deleteEmployer($id)
    {
        $this->getEmployer($id)->delete();
    }

    private function checkValidInput($values){
        if (preg_match('/^[a-zá-ž ]*+$/i', $values->firstname)) {
            if (preg_match('/^[a-zá-ž ]*+$/i', $values->surname)) {
                if (preg_match('/^[1-9][0-9]*$/', $values->salary)) {
                    return true;
                } else {
                    throw new InvalidArgumentException("Plat obsahuje nepovolené znajy");
                }
            } else {
                throw new InvalidArgumentException("Jméno obsahuje nepovolené znaky.");
            }
        } else {
            throw new InvalidArgumentException("Příjmení obsahuje nepovolené znaky.");
        }
    }
}