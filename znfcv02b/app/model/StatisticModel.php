<?php

namespace App\Model;

use Tracy\Debugger;


class StatisticModel extends BaseModel
{

    /**
     * Metoda vrací seznam všech statistik firem, záznam bude mít položky název firmz, minální plat ve firmě, maximální plat ve firmě, průměrný plat a součet všech platů.
     */
    public function listStatistic()
    {
        $values = Array();

        $companies = $this->database->table('company');

        foreach ($companies as $c){
            $values[$c->id]['name']= $c->name;
            $values[$c->id]['min']=$this->database->table('employer')->where('company_id', $c->id)->min('salary');
            $values[$c->id]['max']=$this->database->table('employer')->where('company_id', $c->id)->max('salary');
            $sum = $this->database->table('employer')->where('company_id', $c->id)->sum('salary');
            $n = $this->database->table('employer')->where('company_id', $c->id)->count('*');

            if($n){
                $values[$c->id]['avg']= $sum/$n;
            }else {
                $values[$c->id]['avg']= NULL;
            }

            $values[$c->id]['sum']= $sum;
        }
        return $values;
    }
  }