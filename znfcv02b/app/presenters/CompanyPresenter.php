<?php

namespace App\Presenters;

use App\Model\CompanyModel;
use App\Forms\CompanyFormFactory;
use Nette\Application\UI\Form;
use App\Model\NoDataFound;
use Tracy\Debugger;



class CompanyPresenter extends BasePresenter
{
    /** @var CompanyFormFactory - Formulářová továrnička pro správu firem */
    private $formFactory;

    /** @var CompanyModel - model pro management firem */
    private $companyModel;


    /**
     * Setter pro formulářovou továrničku a model správy firem.
     * @param CompanyFormFactory $formFactory automaticky injectovaná formulářová továrnička pro správu firem
     * @param CompanyModel $companyModel automatiky injetovaný model pro správu firem
     */
    public function injectDependencies(
        CompanyFormFactory $formFactory,
        CompanyModel $companyModel
    )
    {
        $this->formFactory = $formFactory;
        $this->companyModel = $companyModel;
    }


    /**
     * Akce pro editaci
     * @param int $id id firmy
     */
    public function actionEdit($id) {
        $form = $this['editForm'];
        $form->setDefaults($this->companyModel->getCompany($id));
    }

    /**
     * Akce pro mazání
     * @param int $id id firmy
     */
    public function actionDelete($id) {
        $form = $this['deleteForm'];
        $form->setDefaults($this->companyModel->getCompany($id));
    }

    /**
     * Metoda pro vytvoření formuáře pro vložení
     * @return Form - formulář
     */
    public function createComponentAddForm()
    {
        $form = $this->formFactory->createAddForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Company:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuáře pro editaci
     * @return Form - formulář
     */
    public function createComponentEditForm()
    {
        $form = $this->formFactory->createEditForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Company:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuáře pro mazání
     * @return Form - formulář
     */
    public function createComponentDeleteForm()
    {
        $form = $this->formFactory->createDeleteForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Company:default');
        };
        return $form;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderEdit($id) {
        $this->template->name = $this->companyModel->getCompany($id)->name;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDelete($id) {
        $this->template->name = $this->companyModel->getCompany($id)->name;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDefault() {
        $this->template->companies = $this->companyModel->listCompanies();
    }
}