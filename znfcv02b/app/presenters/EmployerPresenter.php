<?php

namespace App\Presenters;


use App\Model\EmployerModel;
use App\Model\CompanyModel;
use App\Forms\EmployerFormFactory;
use Nette\Application\UI\Form;
use App\Model\NoDataFound;
use Tracy\Debugger;



class EmployerPresenter extends BasePresenter
{
    /** @var EmployerFormFactory - Formulářová továrnička pro správu zaměstanců */
    private $formFactory;

    /** @var EmployerModel - model pro management zaměstanců */
    private $employerModel;

    /** @var UserModel - model pro management firem */
    private $companyModel;

    /**
     * Setter pro formulářovou továrničku a modely správy firem a zaměstanců
     * @param UserFormFactory $formFactory automaticky injectovaná formulářová továrnička pro správu zaměstanců
     * @param EmployerModel $employerModel automatiky injetovaný model pro správu zaměstanců
     * @param CompanyModel $companyModel automatiky injetovaný model pro správu firem
     */
    public function injectDependencies(
        EmployerFormFactory $formFactory,
        EmployerModel $employerModel,
        CompanyModel $companyModel
    )
    {
        $this->formFactory = $formFactory;
        $this->employerModel = $employerModel;
        $this->companyModel = $companyModel;
    }

    private function getCompanies(){
        $companies = $this->companyModel->listCompanies();
        $arr = Array();

        foreach ($companies as $c){
            $arr[$c->id]=$c->name;
        }

        return $arr;
    }

    /**
     * Akce pro vkádání
     */
    public function actionAdd() {
        $form = $this['addForm'];
        $form['company_id']->setItems($this->getCompanies());
    }

    /**
     * Akce pro editaci
     * @param int $id id zaměstnance
     */
    public function actionEdit($id) {
        $form = $this['editForm'];
        $form['company_id']->setItems($this->getCompanies());
        $form->setDefaults($this->employerModel->getEmployer($id));
    }

    /**
     * Akce pro mazání
     * @param int $id id zaměstnance
     */
    public function actionDelete($id) {
        $form = $this['deleteForm'];
        $form->setDefaults($this->employerModel->getEmployer($id));
    }

    /**
     * Metoda pro vytvoření formuláře pro vložení
     * @return Form - formulář
     */
    public function createComponentAddForm()
    {
        $form = $this->formFactory->createAddForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuáře pro editaci
     * @return Form - formulář
     */
    public function createComponentEditForm()
    {
        $form = $this->formFactory->createEditForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };
        return $form;
    }

    /**
     * Metoda pro vytvoření formuláře pro mazání
     * @return Form - formulář
     */
    public function createComponentDeleteForm()
    {
        $form = $this->formFactory->createDeleteForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };
        return $form;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderEdit($id) {
        $this->template->name = $this->employerModel->getEmployer($id)->surname;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDelete($id) {
        $this->template->name = $this->employerModel->getEmployer($id)->surname;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDefault() {
        $this->template->employers = $this->employerModel->listEmployers();
    }
}
